# Setup Enviroment

# 1 Create an Account at GitLab  
Link: https://gitlab.com/  

# 2 Download Git and Git extension  
Git: https://git-scm.com/download/win  
Git Extensions: https://sourceforge.net/projects/gitextensions/  

# 3 Install Visual Studio Code:  
https://code.visualstudio.com/

# 4 Add PlatformIo extension to VS Code
4.1 : Go to Extensions and click on it. (Or ptrdd Ctrl + shift + x)  
4.2 : Type PlatformIO to the searching box  
4.3 : Click on PlatformIO IDE and click on instal. Meanwhile it is installing you can read the project's details.  

![](pics/01.png)

4.4 : Restart VS Code